package com.atlassian.soy.renderer;

/**
 * @since 1.1
 */
public class JsExpression
{

    private final String text;

    public JsExpression(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
