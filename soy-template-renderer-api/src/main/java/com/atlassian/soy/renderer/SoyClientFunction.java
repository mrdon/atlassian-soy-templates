package com.atlassian.soy.renderer;


/**
 * An soy function which can be invoked on the client.
 * @since 1.1
 */
public interface SoyClientFunction extends SoyFunction
{
    /**
     * Generates a JavasScript expression from the given JavaScript expressions.
     * <p/>
     * This is differs from {@link SoyServerFunction} as it deals with runtime values where as this
     * method deals with JavaScript expressions at template compile time
     * @param args the args for the function. The number of args is guaranteed
     *             to be one of valid arg sizes supplied by {@link #validArgSizes()}
     * @return a JavaScript expression
     */
    JsExpression generate(JsExpression... args);
}
