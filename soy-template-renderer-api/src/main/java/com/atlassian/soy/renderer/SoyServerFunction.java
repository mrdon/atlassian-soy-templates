package com.atlassian.soy.renderer;

/**
 * An soy function which can be invoked on the server.
 * @param <T> the type of the value returned
 * @since 1.1
 */
public interface SoyServerFunction<T> extends SoyFunction
{
    /**
     * @param args the args for the function. The number of args is guaranteed
     *             to be one of valid arg sizes supplied by {@link #validArgSizes()}
     * @return the value computed from the args.
     */
    T apply(Object... args);
}
