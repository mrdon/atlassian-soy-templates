package com.atlassian.soy.renderer;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

public class SoyFunctionModuleDescriptor extends AbstractModuleDescriptor<SoyFunction>
{
    public static final String XML_ELEMENT_NAME = "soy-function";

    private SoyFunction module;

    public SoyFunctionModuleDescriptor(ModuleFactory factory)
    {
        super(factory);
    }

    @Override
    public void enabled()
    {
        super.enabled();
        module = moduleFactory.createModule(moduleClassName, this);
    }

    @Override
    public void disabled()
    {
        super.disabled();
        module = null;
    }

    @Override
    public SoyFunction getModule()
    {
        return module;
    }
    
}
