package com.atlassian.soy.impl.data;

import com.atlassian.soy.impl.data.EnumData;
import com.atlassian.soy.impl.data.SoyDataConverter;
import com.atlassian.soy.renderer.CustomSoyDataMapper;
import com.atlassian.soy.renderer.SoyDataMapper;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.data.restricted.BooleanData;
import com.google.template.soy.data.restricted.FloatData;
import com.google.template.soy.data.restricted.IntegerData;
import com.google.template.soy.data.restricted.NullData;
import com.google.template.soy.data.restricted.StringData;
import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SoyDataConverterTest
{
    @Mock
    private SoyDataMapper soyDataMapper;

    private SoyDataConverter soyDataConverter;

    @CustomSoyDataMapper("test-name")
    public static class TestCustomObject
    {}

    public static class ObjectWithProperty
    {
        public String getValue() { return "Cheese"; }
    }

    public enum TestEnum
    {
        SINGLETON
    }

    @Before
    public void initialiseConverter()
    {
        soyDataConverter = new SoyDataConverter(Collections.singletonList(soyDataMapper));
    }

    @After
    public void cleanUpMembers()
    {
        soyDataConverter = null;
    }

    @Test
    public void testConvertToSoyData() throws Exception
    {
        assertConvertsToType("", StringData.class);
        assertConvertsToType(TestEnum.SINGLETON, EnumData.class);

        assertConvertsToType(5, IntegerData.class);
        assertConvertsToType(Integer.valueOf(5).shortValue(), IntegerData.class);
        assertConvertsToType(Integer.valueOf(5).byteValue(), IntegerData.class);
        
        assertConvertsToType(Integer.valueOf(Integer.MAX_VALUE).longValue(), IntegerData.class);
        assertConvertsToType(Integer.valueOf(Integer.MIN_VALUE).longValue(), IntegerData.class);
        assertConvertsToType(Integer.valueOf(0).longValue(), IntegerData.class);
        
        assertConvertsToType(Long.MAX_VALUE, FloatData.class);

        assertConvertsToType(true, BooleanData.class);
        assertConvertsToType(false, BooleanData.class);
        assertConvertsToType((Object) null, NullData.class);
        
        assertConvertsToType(Collections.emptyList(), SoyListData.class);
        assertConvertsToType(Collections.emptySet(), SoyListData.class);
        assertConvertsToType(new Object[0], SoyListData.class);
        assertConvertsToType(new String[0], SoyListData.class);

        assertConvertsToType(Collections.emptyMap(), SoyMapData.class);
    }

    @Test
    public void testCustomSoyMapper() throws Exception
    {
        TestCustomObject custom = new TestCustomObject();

        when(soyDataMapper.getName()).thenReturn("test-name");
        when(soyDataMapper.convert(custom)).thenReturn("Response");

        assertEquals(StringData.forValue("Response"), soyDataConverter.convertToSoyData(custom));
    }

    @Test
    public void testCustomSoyMapperLazyEvaluation()
    {
        TestCustomObject custom = new TestCustomObject();
        final AtomicBoolean called = new AtomicBoolean(false);

        when(soyDataMapper.getName()).thenReturn("test-name");
        when(soyDataMapper.convert(custom)).thenReturn(new Callable<ObjectWithProperty>() {
            @Override
            public ObjectWithProperty call() throws Exception
            {
                called.set(true);
                return new ObjectWithProperty();
            }
        });

        SoyMapData result = (SoyMapData) soyDataConverter.convertToSoyData(custom);
        assertFalse(called.get());

        assertEquals(StringData.forValue("Cheese"), result.get("value"));
    }

    @Test(expected = IllegalStateException.class)
    public void testCustomSoyMapperNotFound() throws Exception
    {
        when(soyDataMapper.getName()).thenReturn("not-matching");
        soyDataConverter.convertToSoyData(new TestCustomObject());
    }

    @Test
    public void testConvertFromSoyData() throws Exception
    {
        assertConvertsToType(StringData.forValue(""), String.class);
        assertConvertsToType(new EnumData(TestEnum.SINGLETON), TestEnum.class);

        assertConvertsToType(IntegerData.forValue(0), Integer.class);

        assertConvertsToType(FloatData.forValue(0), Double.class);

        assertConvertsToType(BooleanData.TRUE, Boolean.class);

        assertNull(SoyDataConverter.convertFromSoyData(NullData.INSTANCE));

        assertConvertsToType(new SoyListData(1, 2), List.class);

        assertConvertsToType(new SoyMapData(Collections.singletonMap("key", 1)), Map.class);
    }

    public void assertConvertsToType(SoyData value, Class<?> type)
    {
        final Object object = SoyDataConverter.convertFromSoyData(value);
        assertThat(object, Is.is(type));
    }
    
    public void assertConvertsToType(Object value, Class<? extends SoyData> type)
    {
        final SoyData soyData = soyDataConverter.convertToSoyData(value);
        assertNotNull(soyData);
        assertThat(soyData, Is.is(type));
    }
}
