package com.atlassian.soy.impl.functions;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.web.context.HttpContext;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ContextFunctionTest
{
    private static final List<JsExpr> EMPTY_JS_ARGS = Collections.emptyList();
    private static final List<SoyData> EMPTY_TOFU_ARGS = Collections.emptyList();

    private ContextFunction toTest;

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private HttpContext httpContext;

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);
        toTest = new ContextFunction(applicationProperties, httpContext);
        when(httpContext.getRequest()).thenReturn(null);
    }

    @Test
    public void baseUrlIsEmpty()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("");
        assertEquals("\"\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("", toTest.computeForTofu(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlIsJustSlash()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("/");
        assertEquals("\"\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("", toTest.computeForTofu(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlHasNoTrailingSlash()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("foo");
        assertEquals("\"foo\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("foo", toTest.computeForTofu(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlHasTrailingSlash()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("foo/");
        assertEquals("\"foo\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("foo", toTest.computeForTofu(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlHasLeadingAndTrailingSlashes()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("/foo/");
        assertEquals("\"/foo\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("/foo", toTest.computeForTofu(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void updatedBaseUrlIsGenerated()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("foo", "bar");
        JsExpr jsResult1 = toTest.computeForJsSrc(EMPTY_JS_ARGS);
        JsExpr jsResult2 = toTest.computeForJsSrc(EMPTY_JS_ARGS);
        assertEquals("\"foo\"", jsResult1.getText());
        assertEquals("\"bar\"", jsResult2.getText());
    }
}
