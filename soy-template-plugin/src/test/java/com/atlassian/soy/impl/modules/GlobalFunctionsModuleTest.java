package com.atlassian.soy.impl.modules;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.soy.impl.data.SoyDataConverter;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyDataMapper;
import com.atlassian.soy.renderer.SoyFunctionModuleDescriptor;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.template.soy.SoyFileSet;
import com.google.template.soy.SoyModule;
import com.google.template.soy.jssrc.SoyJsSrcOptions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Tests the GlobalFunctionsModule creates working functions from the pluginAccessor.
 */
@RunWith(MockitoJUnitRunner.class)
public class GlobalFunctionsModuleTest
{
    private static final String FOO_TEMPLATE_WITH_FUNC_OUTPUT = "// This file was automatically generated from foo.soy.\n" +
        "// Please don't edit this file by hand.\n" +
        "\n" +
        "if (typeof foo == 'undefined') { var foo = {}; }\n" +
        "\n" +
        "\n" +
        "foo.bar = function(opt_data, opt_sb) {\n" +
        "  var output = opt_sb || new soy.StringBuilder();\n" +
        "  output.append(soy.$$escapeHtml(MYJS));\n" +
        "  return opt_sb ? '' : output.toString();\n" +
        "};\n";

    @Mock private PluginAccessor pluginAccessor;
    @Mock private ModuleFactory moduleFactory;

    private GlobalFunctionsModule globalFunctionsModule;

    @Before
    public void setUp()
    {
        SoyDataConverter soyDataConverter = new SoyDataConverter(Collections.<SoyDataMapper>emptyList());
        globalFunctionsModule = new GlobalFunctionsModule(soyDataConverter, pluginAccessor);
    }

    @Test
    public void testGlobalFunctionWorks()
    {
        SoyFunctionModuleDescriptor fooFunctionDescriptor = new SoyFunctionModuleDescriptor(moduleFactory);
        when(moduleFactory.createModule(anyString(), eq(fooFunctionDescriptor))).thenReturn(makeJsFunc("foo"));
        fooFunctionDescriptor.enabled();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(SoyFunctionModuleDescriptor.class)).thenReturn(Collections.singletonList(fooFunctionDescriptor));

        Injector injector = Guice.createInjector(globalFunctionsModule, new SoyModule());
        SoyFileSet.Builder builder = injector.getInstance(SoyFileSet.Builder.class);

        List<String> out = builder.add("{namespace foo}\n/** */\n{template .bar}\n{foo()}\n{/template}\n", "foo.soy").build().compileToJsSrc(new SoyJsSrcOptions(), null);
        assertEquals(FOO_TEMPLATE_WITH_FUNC_OUTPUT, out.get(0));
    }

    private SoyClientFunction makeJsFunc(final String functionName)
    {
        return new SoyClientFunction()
        {
            @Override
            public String getName()
            {
                return functionName;
            }

            @Override
            public JsExpression generate(JsExpression... args)
            {
                return new JsExpression("MYJS");
            }

            @Override
            public Set<Integer> validArgSizes()
            {
                return ImmutableSet.of(0);
            }
        };
    }

}
