package com.atlassian.soy.impl.modules;

import com.google.inject.OutOfScopeException;
import com.google.template.soy.shared.internal.GuiceSimpleScope;
import org.junit.Test;

import java.util.concurrent.Callable;

import static org.junit.Assert.*;

public class GuiceStackingScopeTest
{

    private GuiceSimpleScope scope = new GuiceStackingScope();

    @Test
    public void testIsActive() throws Exception
    {
        assertFalse("Should be inactive before entering first scope", scope.isActive());
        scope.enter();
        assertTrue("Should be active after entering first scope", scope.isActive());
        scope.enter();
        assertTrue("Should be active after entering second scope", scope.isActive());
        scope.exit();
        assertTrue("Should be active after exiting second scope", scope.isActive());
        scope.exit();
        assertFalse("Should be inactive after exiting first scope", scope.isActive());
    }

    @Test
    public void testSeed() throws Exception
    {
        assertThrows(OutOfScopeException.class, callableGetForTesting(scope, Integer.class));
        scope.enter();
        scope.seed(Integer.class, 1);
        assertEquals("value should be 1 in first scope map", 1, (int) scope.getForTesting(Integer.class));
        scope.enter();
        scope.seed(Integer.class, 2);
        assertEquals("value should be 2 in first scope map", 2, (int) scope.getForTesting(Integer.class));
        scope.exit();
        assertEquals("value should be 1 in first scope map", 1, (int) scope.getForTesting(Integer.class));
        scope.exit();
        assertThrows(OutOfScopeException.class, callableGetForTesting(scope, Integer.class));
    }

    private <T> Callable<T> callableGetForTesting(final GuiceSimpleScope scope, final Class<T> klass)
    {
        return new Callable<T>()
        {
            public T call()
            {
                return scope.getForTesting(klass);
            }
        };
    }

    public void assertThrows(Class<? extends Exception> exceptionClass, Callable callable)
    {
        try
        {
            callable.call();
            fail("should have thrown an exception which is an instanceof " + exceptionClass.getCanonicalName());
        }
        catch (Throwable e)
        {
            assertTrue("Should be an instanceof " + exceptionClass.getCanonicalName(), exceptionClass.isInstance(e));
        }
    }
}
