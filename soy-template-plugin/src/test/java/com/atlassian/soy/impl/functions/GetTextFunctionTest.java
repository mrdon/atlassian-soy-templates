package com.atlassian.soy.impl.functions;

import com.atlassian.sal.api.message.I18nResolver;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.restricted.StringData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.Serializable;
import java.util.Arrays;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetTextFunctionTest {

    @Mock
    private I18nResolver i18nResolver;

    private GetTextFunction function;

    @Before
    public void setUp() throws Exception
    {
        doReturn("").when(i18nResolver).getText(anyString(), Matchers.<Serializable>anyVararg());
        function = new GetTextFunction(i18nResolver);
    }

    @Test
    public void testComputeForTofu() throws Exception
    {
        function.computeForTofu(Arrays.<SoyData>asList(StringData.forValue("ma.key"), StringData.forValue("blah")));
        verify(i18nResolver).getText("ma.key", "blah");
    }
}
