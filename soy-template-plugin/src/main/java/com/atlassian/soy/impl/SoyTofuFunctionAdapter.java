package com.atlassian.soy.impl;

import com.atlassian.soy.impl.data.SoyDataConverter;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.tofu.restricted.SoyTofuFunction;

import java.util.List;
import java.util.Set;

public class SoyTofuFunctionAdapter implements SoyTofuFunction {

    private final SoyServerFunction<?> soyServerFunction;
    private final SoyDataConverter soyDataConverter;

    public SoyTofuFunctionAdapter(SoyServerFunction soyServerFunction, SoyDataConverter soyDataConverter) {
        this.soyServerFunction = soyServerFunction;
        this.soyDataConverter = soyDataConverter;
    }

    @Override
    public SoyData computeForTofu(List<SoyData> args) {
        final Object[] pluginArgs = Lists.transform(args, new Function<SoyData, Object>() {
            public Object apply(SoyData from) {
                return soyDataConverter.convertFromSoyData(from);
            }
        }).toArray();
        return soyDataConverter.convertToSoyData(soyServerFunction.apply(pluginArgs));
    }

    @Override
    public String getName() {
        return soyServerFunction.getName();
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return soyServerFunction.validArgSizes();
    }
}
