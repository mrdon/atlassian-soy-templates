package com.atlassian.soy.impl;

import com.atlassian.soy.renderer.SoyException;
import com.google.template.soy.SoyFileSet;

import java.util.Map;

public interface SoyManager
{
    /**
     * Make a SoyFileSet builder, loading soy functions and templates from the provided modules. Modules may be
     * soy-resource modules (loading functions and templates directly) or web-resource modules (loading any
     * functions or templates referenced by one of the modules dependencies)
     *
     * @return a new Soy builder instance for that module
     * @throws com.google.common.collect.ComputationException if an error occurs in loading functions from the given modules
     */
    public SoyFileSet.Builder makeSoyFileSetBuilder();

    /**
     * Render a template from a module key into an appendable.
     * @param appendable the appendable to write to
     * @param completeModuleKey - a complete plugin module key containing the template resource
     * @param templateName - a namespaced Soy template name
     * @param data - a map of data to render the template with
     * @param injectedData - a map of injected data to render the template with
     *
     * @throws SoyException when an error occurs in rendering the template
     */
    void render(Appendable appendable, String completeModuleKey, String templateName,
                Map<String, Object> data, Map<String, Object> injectedData) throws SoyException;
}
