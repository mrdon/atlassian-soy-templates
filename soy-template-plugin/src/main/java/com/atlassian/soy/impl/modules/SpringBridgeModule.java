package com.atlassian.soy.impl.modules;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.web.context.HttpContext;
import com.google.inject.AbstractModule;

/**
 * Provides the Spring beans for the soy functions
 */
class SpringBridgeModule extends AbstractModule
{

    private final ApplicationProperties applicationProperties;
    private final I18nResolver i18nResolver;
    private final WebResourceManager webResourceManager;
    private final HttpContext httpContext;

    public SpringBridgeModule(ApplicationProperties applicationProperties,
                              I18nResolver i18nResolver,
                              WebResourceManager webResourceManager,
                              HttpContext httpContext)
    {
        this.applicationProperties = applicationProperties;
        this.i18nResolver = i18nResolver;
        this.webResourceManager = webResourceManager;
        this.httpContext = httpContext;
    }

    @Override
    public void configure()
    {
        binder().bind(I18nResolver.class).toInstance(i18nResolver);
        binder().bind(ApplicationProperties.class).toInstance(applicationProperties);
        binder().bind(WebResourceManager.class).toInstance(webResourceManager);
        binder().bind(HttpContext.class).toInstance(httpContext);
    }
}
