package com.atlassian.soy.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Determines and caches the appropriate set of template file URLs for different sets of plugin modules.
 */
class TemplateSetFactory
{
    private static final Logger log = LoggerFactory.getLogger(TemplateSetFactory.class);

    private final PluginAccessor pluginAccessor;
    private final ServletContextFactory servletContextFactory;

    /**
     * Requisite config so we can make an Injector and a Builder without parsing all the modules twice.
     */
    private final Cache<String, Set<URL>> templateSetCache;

    TemplateSetFactory(PluginAccessor pluginAccessor, ServletContextFactory servletContextFactory,
                       int injectorCacheExpiryTime, TimeUnit injectorCacheExpiryTimeUnit)
    {
        this.pluginAccessor = pluginAccessor;
        this.servletContextFactory = servletContextFactory;
        CacheLoader<String, Set<URL>> findTemplatesFunction = new CacheLoader<String, Set<URL>>()
        {
            @Override
            public Set<URL> load(String moduleKey)
            {
                return findRequiredTemplates(moduleKey);
            }
        };

        templateSetCache = CacheBuilder.newBuilder()
            .expireAfterWrite(injectorCacheExpiryTime, injectorCacheExpiryTimeUnit)
            .build(findTemplatesFunction);
    }

    /**
     * Retrieve the template files applicable to a given set of plugin module keys.
     * The templates can be extracted from WebResources and SoyWebResources in the dependency
     * trees of the given plugin module.
     */
    Set<URL> get(String pluginModulesKey)
    {
        return templateSetCache.getUnchecked(pluginModulesKey);
    }

    void clear()
    {
        templateSetCache.invalidateAll();
    }

    private Set<URL> findRequiredTemplates(String pluginModuleKey)
    {
        log.debug("Found Soy template files for '{}'", pluginModuleKey);
        TemplateSetBuilder templateSetBuilder = new TemplateSetBuilder();

        templateSetBuilder.addTemplatesForTree(pluginModuleKey);
        Set<URL> result = templateSetBuilder.build();
        log.debug("Found Soy template files for '{}' was {}", pluginModuleKey, result);
        return result;
    }

    private class TemplateSetBuilder
    {
        private final Set<URL> fileSet = Sets.newHashSet();
        private final Set<String> alreadyAddedModules = Sets.newHashSet();

        private void addTemplatesForTree(String completeModuleKey)
        {
            if (alreadyAddedModules.contains(completeModuleKey))
                return;

            ModuleDescriptor<?> moduleDescriptor = pluginAccessor.getEnabledPluginModule(completeModuleKey);

            if (moduleDescriptor == null)
                throw new IllegalStateException("Required plugin module " + completeModuleKey + " was either missing or disabled");

            if (moduleDescriptor instanceof WebResourceModuleDescriptor)
                addTemplatesForTree((WebResourceModuleDescriptor) moduleDescriptor);

            addSoyTemplateResources(moduleDescriptor);

            alreadyAddedModules.add(completeModuleKey);
        }

        private void addTemplatesForTree(WebResourceModuleDescriptor webResourceModuleDescriptor)
        {
            for (String dependencyModuleKey : webResourceModuleDescriptor.getDependencies())
            {
                addTemplatesForTree(dependencyModuleKey);
            }
        }

        private void addSoyTemplateResources(ModuleDescriptor<?> moduleDescriptor)
        {
            for (ResourceDescriptor resource : moduleDescriptor.getResourceDescriptors())
            {
                if (isSoyTemplate(resource))
                {
                    URL url = getSoyResourceURL(moduleDescriptor, resource);
                    if (url != null)
                    {
                        this.fileSet.add(url);
                    }
                }
            }
        }

        private boolean isSoyTemplate(ResourceDescriptor resource)
        {
            return resource.getLocation().endsWith(".soy");
        }

        private URL getSoyResourceURL(ModuleDescriptor moduleDescriptor, ResourceDescriptor resource)
        {
            final String sourceParam = resource.getParameter("source");
            if ("webContextStatic".equalsIgnoreCase(sourceParam))
            {
                try
                {
                    return servletContextFactory.getServletContext().getResource(resource.getLocation());
                }
                catch (MalformedURLException e)
                {
                    log.error("Ignoring soy resource. Could not locate soy with location: " + resource.getLocation());
                    return null;
                }
            }
            return moduleDescriptor.getPlugin().getResource(resource.getLocation());
        }

        private Set<URL> build()
        {
            return ImmutableSet.copyOf(fileSet);
        }
    }
}