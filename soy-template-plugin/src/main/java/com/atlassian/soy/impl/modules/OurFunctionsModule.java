package com.atlassian.soy.impl.modules;

import com.atlassian.soy.impl.functions.*;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.template.soy.shared.restricted.SoyFunction;

/**
 * Our custom soy functions are added here
 */
class OurFunctionsModule extends AbstractModule
{
    @Override
    public void configure()
    {
        Multibinder<SoyFunction> binder = Multibinder.newSetBinder(binder(), SoyFunction.class);
        binder.addBinding().to(ContextFunction.class);
        binder.addBinding().to(GetTextFunction.class);
        binder.addBinding().to(IncludeResourcesFunction.class);
        binder.addBinding().to(RequireResourceFunction.class);
        binder.addBinding().to(RequireResourcesForContextFunction.class);
    }
}
