package com.atlassian.soy.impl;

/**
 * Are we in dev mode?
 */
public class DevMode
{
    private static final String JIRA_DEV_MODE = "jira.dev.mode";
    private static final String ATLASSIAN_DEV_MODE = "atlassian.dev.mode";

    public static boolean isDevMode()
    {
        return Boolean.getBoolean(JIRA_DEV_MODE) || Boolean.getBoolean(ATLASSIAN_DEV_MODE);

    }
}
