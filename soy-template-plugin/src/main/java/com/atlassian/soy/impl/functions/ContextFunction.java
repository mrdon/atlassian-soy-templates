package com.atlassian.soy.impl.functions;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.web.context.HttpContext;
import com.google.common.collect.ImmutableSet;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.internal.base.CharEscaper;
import com.google.template.soy.internal.base.CharEscapers;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.tofu.restricted.SoyTofuFunction;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

/**
 * Simple function that returns the current request context to the caller.
 *
 * @since v4.4
 */
@Singleton
public class ContextFunction implements SoyJsSrcFunction, SoyTofuFunction
{
    private final ApplicationProperties applicationProperties;
	private final HttpContext httpContext;

    @Inject
    public ContextFunction(ApplicationProperties applicationProperties, HttpContext httpContext)
    {
        this.applicationProperties = applicationProperties;
		this.httpContext = httpContext;
    }

    private static String stripTrailingSlash(String base)
    {
        return StringUtils.chomp(base, "/");
    }

    @Override
    public String getName()
    {
        return "contextPath";
    }

    @Override
    public Set<Integer> getValidArgsSizes()
    {
        return ImmutableSet.of(0);
    }

    @Override
    public JsExpr computeForJsSrc(List<JsExpr> args)
    {
        CharEscaper jsEscaper = CharEscapers.javascriptEscaper();
        return new JsExpr('"' + jsEscaper.escape(getContextPath()) + '"', Integer.MAX_VALUE);
    }

    @Override
    public SoyData computeForTofu(List<SoyData> soyDatas)
    {
        return StringData.forValue(getContextPath());
    }

    private String getContextPath()
    {
        HttpServletRequest request = httpContext.getRequest();
        return request != null ? request.getContextPath() : getContextPathFromBaseUrl();
    }

    private String getContextPathFromBaseUrl()
    {
        String baseUrl = stripTrailingSlash(applicationProperties.getBaseUrl());
        try
        {
            return new URI(baseUrl).getPath();
        }
        catch (URISyntaxException e)
        {
            throw new RuntimeException(e);
        }
    }
}
