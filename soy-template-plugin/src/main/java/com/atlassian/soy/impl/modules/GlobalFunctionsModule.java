package com.atlassian.soy.impl.modules;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.soy.impl.CompositeFunctionAdaptor;
import com.atlassian.soy.impl.SoyJsSrcFunctionAdapter;
import com.atlassian.soy.impl.SoyTofuFunctionAdapter;
import com.atlassian.soy.impl.data.SoyDataConverter;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyFunctionModuleDescriptor;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.template.soy.shared.restricted.SoyFunction;

/**
 * Loads functions from a pluginAccessor.
 */
class GlobalFunctionsModule extends AbstractModule
{
    private final SoyDataConverter soyDataConverter;
    private final PluginAccessor pluginAccessor;

    public GlobalFunctionsModule(SoyDataConverter soyDataConverter, PluginAccessor pluginAccessor)
    {
        this.soyDataConverter = soyDataConverter;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public void configure()
    {
        Multibinder<SoyFunction> binder = Multibinder.newSetBinder(binder(), SoyFunction.class);
        for (SoyFunctionModuleDescriptor md : pluginAccessor.getEnabledModuleDescriptorsByClass(SoyFunctionModuleDescriptor.class))
        {
            final com.atlassian.soy.renderer.SoyFunction function = md.getModule();
            if (function instanceof SoyServerFunction)
            {
                if (function instanceof SoyClientFunction)
                {
                    binder.addBinding().toInstance(new CompositeFunctionAdaptor(function, soyDataConverter));
                }
                else
                {
                    binder.addBinding().toInstance(new SoyTofuFunctionAdapter((SoyServerFunction) function, soyDataConverter));
                }
            }
            else if (function instanceof SoyClientFunction)
            {
                binder.addBinding().toInstance(new SoyJsSrcFunctionAdapter((SoyClientFunction) function));
            }
        }
    }
}
