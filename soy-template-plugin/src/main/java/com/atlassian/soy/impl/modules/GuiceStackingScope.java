package com.atlassian.soy.impl.modules;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.Key;
import com.google.inject.OutOfScopeException;
import com.google.inject.Provider;
import com.google.template.soy.shared.internal.GuiceSimpleScope;

import java.util.Deque;
import java.util.Map;

import static com.google.common.base.Preconditions.checkState;

class GuiceStackingScope extends GuiceSimpleScope
{

    private final ThreadLocal<Deque<Map<Key<?>, Object>>> stackedThreadLocal =
            new ThreadLocal<Deque<Map<Key<?>, Object>>>();

    @Override
    public void enter()
    {
        if (!isActive())
        {
            stackedThreadLocal.set(Lists.<Map<Key<?>, Object>>newLinkedList());
        }
        stackedThreadLocal.get().push(Maps.<Key<?>, Object>newHashMap());
    }

    @Override
    public void exit()
    {
        stackedThreadLocal.get().pop();
        if (!isActive())
        {
            stackedThreadLocal.remove();
        }
    }

    @Override
    public boolean isActive()
    {
        Deque<Map<Key<?>, Object>> stack = stackedThreadLocal.get();
        return stack != null && !stack.isEmpty();
    }

    @Override
    public <T> void seed(Key<T> key, T value)
    {
        // copied from super class but uses subclasses getScopedValues
        Map<Key<?>, Object> scopedObjects = getScopedValues(key);
        checkState(!scopedObjects.containsKey(key),
                "A value for the key %s was already seeded in this scope. Old value: %s New value: %s",
                key, scopedObjects.get(key), value);
        scopedObjects.put(key, value);
    }

    @Override
    public <T> T getForTesting(Key<T> key)
    {
        // copied from super class but uses subclasses getScopedValues
        Map<Key<?>, Object> scopedValues = getScopedValues(key);
        @SuppressWarnings("unchecked")
        T value = (T) scopedValues.get(key);
        if (value == null && !scopedValues.containsKey(key)) {
            throw new IllegalStateException("The key " + key + " has not been seeded in this scope");
        }
        return value;
    }

    @Override
    public <T> Provider<T> scope(final Key<T> key, final Provider<T> unscopedProvider)
    {
        // copied from super class but uses subclasses getScopedValues
        return new Provider<T>()
        {
            @Override
            public T get()
            {
                Map<Key<?>, Object> scopedValues = getScopedValues(key);
                @SuppressWarnings("unchecked")
                T value = (T) scopedValues.get(key);
                if (value == null && !scopedValues.containsKey(key))
                {
                    value = unscopedProvider.get();
                    scopedValues.put(key, value);
                }
                return value;
            }
        };
    }

    /**
     * helper to get the map of scoped values specific to the current thread and render context.
     * @param key The key that is intended to be retrieved from the returned map.
     */
    protected <T> Map<Key<?>, Object> getScopedValues(Key<T> key)
    {
        if (!isActive())
        {
            throw new OutOfScopeException("Cannot access " + key + " outside of a scoping block");
        }
        return stackedThreadLocal.get().peek();
    }

}
