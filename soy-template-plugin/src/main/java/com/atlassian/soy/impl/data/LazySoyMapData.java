package com.atlassian.soy.impl.data;

import com.atlassian.util.concurrent.LazyReference;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.data.restricted.NullData;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;

class LazySoyMapData extends SoyMapData
{
    private final Cache<String, SoyData> cache;
    private final LazyReference<Object> delegate;

    LazySoyMapData(final Callable<Object> objectSource, final SoyDataConverter soyDataConverter)
    {
        super();
        cache = CacheBuilder.newBuilder()
            .build(new CacheLoader<String, SoyData>()
            {
                @Override
                public SoyData load(final String from)
                {
                    Object value;
                    try
                    {
                        value = "class".equals(from) ? null : PropertyUtils.getProperty(delegate.get(), from);
                    }
                    catch (IllegalAccessException e)
                    {
                        throw new RuntimeException(e);
                    }
                    catch (InvocationTargetException e)
                    {
                        throw new RuntimeException(e);
                    }
                    catch (NoSuchMethodException e)
                    {
                        value = null;
                    }
                    return SoyData.createFromExistingData(soyDataConverter.convertObject(value));
                }
            });

        this.delegate = new LazyReference<Object>()
        {
            @Override
            protected Object create() throws Exception
            {
                return objectSource.call();
            }
        };
    }

    public Object getDelegate()
    {
        return delegate.get();
    }

    @Override
    public SoyData getSingle(String key)
    {
        SoyData soyData = super.getSingle(key);
        return soyData != null && soyData != NullData.INSTANCE ? soyData : cache.getUnchecked(key);
    }

    @Override
    public String toString()
    {
        return getDelegate().toString();
    }

}
