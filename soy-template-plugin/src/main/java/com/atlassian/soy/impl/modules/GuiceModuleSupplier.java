package com.atlassian.soy.impl.modules;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.soy.impl.data.SoyDataConverter;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.inject.Module;
import com.google.inject.util.Modules;
import com.google.template.soy.SoyModule;
import com.google.template.soy.xliffmsgplugin.XliffMsgPluginModule;

public class GuiceModuleSupplier implements Supplier<Iterable<Module>>
{

    private final Iterable<Module> defaultModules;


    public GuiceModuleSupplier(
            ApplicationProperties applicationProperties,
            I18nResolver i18nResolver,
            PluginAccessor pluginAccessor,
            SoyDataConverter soyDataConverter,
            WebResourceManager webResourceManager,
            HttpContext httpContext)
    {
        this.defaultModules = ImmutableList.<Module>builder()
                .add(Modules.override(new SoyModule()).with(new StackingScopeModule()))
                .add(new XliffMsgPluginModule())
                .add(new SpringBridgeModule(applicationProperties, i18nResolver, webResourceManager, httpContext))
                .add(new OurFunctionsModule())
                .add(new GlobalFunctionsModule(soyDataConverter, pluginAccessor))
                .build();
    }

    @Override
    public Iterable<Module> get()
    {
        return defaultModules;
    }
}
