package com.atlassian.soy.impl.data;

import com.atlassian.soy.renderer.CustomSoyDataMapper;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.Callables;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.data.restricted.BooleanData;
import com.google.template.soy.data.restricted.FloatData;
import com.google.template.soy.data.restricted.IntegerData;
import com.google.template.soy.data.restricted.NullData;
import com.google.template.soy.data.restricted.StringData;

import com.atlassian.soy.renderer.SoyDataMapper;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Converts a Map of general objects into a SoyMapData instance.
 */
public class SoyDataConverter
{
    private final List<SoyDataMapper> customMappers;

    public SoyDataConverter(List<SoyDataMapper> customMappers)
    {
        this.customMappers = customMappers;
    }

    public <K, V> SoyMapData convertToSoyMapData(Map<K, V> map)
    {
        SoyMapData soyMapData = new SoyMapData();
        for (Map.Entry<K, V> entry : map.entrySet())
        {
            String key = entry.getKey().toString();
            Object value = entry.getValue();
            Object soyValue = convertObject(value);
            soyMapData.put(key, soyValue);
        }
        return soyMapData;
    }

    public <V> SoyListData convertToSoyListData(Iterable<V> list)
    {
        SoyListData listData = new SoyListData();
        for (Object o : list)
        {
            listData.add(convertObject(o));
        }
        return listData;
    }

    public SoyData convertToSoyData(Object value)
    {
        return SoyData.createFromExistingData(convertObject(value));
    }

    @SuppressWarnings ( { "unchecked" })
    Object convertObject(Object value)
    {
        if (value == null
                || value instanceof Boolean
                || value instanceof Integer
                || value instanceof Double
                || value instanceof Float
                || value instanceof String)
        {
            return value;
        }
        else if (value instanceof Short
                || value instanceof Byte)
        {
            return ((Number) value).intValue();
        }
        else if (value instanceof Long)
        {
            final Long longValue = (Long) value;
            if (longValue >= Integer.MIN_VALUE && longValue <= Integer.MAX_VALUE)
            {
                return longValue.intValue();
            }
            else
            {
                // Could throw an exception... or make a best effort. Unlikely to happen?
                return longValue.doubleValue();
            }
        }
        else if (value instanceof Map)
        {
            return convertToSoyMapData((Map<Object, Object>) value);
        }
        else if (value instanceof Iterable)
        {
            return convertToSoyListData((Iterable<Object>) value);
        }
        else if (value.getClass().isArray())
        {
            // Should we support primitive arrays?
            return convertToSoyListData(Arrays.asList((Object[]) value));
        }
        else if (value instanceof Enum)
        {
            return new EnumData((Enum) value);
        }
        else
        {
            CustomSoyDataMapper mapper = value.getClass().getAnnotation(CustomSoyDataMapper.class);
            if (mapper != null)
            {
                return customConvert(value, mapper.value());
            }
            else
            {
                // Some general object. Inspect it into a map, lazily
                return new LazySoyMapData(makeCallable(value), this);
            }
        }
    }

    private Callable makeCallable(final Object value)
    {
        if (value instanceof Callable)
        {
            return (Callable)value;
        }

        return Callables.returning(value);
    }

    private Object customConvert(Object object, String mapper)
    {
        for (SoyDataMapper customMapper : customMappers)
        {
            if (customMapper.getName().equals(mapper))
            {
                //noinspection unchecked
                return convertToSoyData(customMapper.convert(object));
            }
        }

        throw new IllegalStateException("Could not find custom mapper " + mapper + " for class " + object.getClass());
    }

    public static Object convertFromSoyData(SoyData data)
    {
        if (data instanceof LazySoyMapData)
        {
            return ((LazySoyMapData) data).getDelegate();
        }
        else if (data instanceof SoyMapData)
        {
            return Maps.transformValues(((SoyMapData) data).asMap(), new Function<SoyData, Object>()
            {
                @Override
                public Object apply(SoyData from)
                {
                    return convertFromSoyData(from);
                }
            });
        }
        else if (data instanceof SoyListData)
        {
            return Lists.transform(((SoyListData) data).asList(), new Function<SoyData, Object>()
            {
                @Override
                public Object apply(SoyData from)
                {
                    return convertFromSoyData(from);
                }
            });
        }
        else if (data instanceof EnumData)
        {
            return ((EnumData) data).getValue();
        }
        else if (data instanceof StringData)
        {
            return data.stringValue();
        }
        else if (data instanceof IntegerData)
        {
            return ((IntegerData) data).getValue();
        }
        else if (data instanceof BooleanData)
        {
            return ((BooleanData) data).getValue();
        }
        else if (data instanceof FloatData)
        {
            return ((FloatData) data).getValue();
        }
        else
        {
            return data == NullData.INSTANCE ? null : data.stringValue();
        }
    }

}