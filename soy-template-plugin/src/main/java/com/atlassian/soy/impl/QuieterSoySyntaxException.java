package com.atlassian.soy.impl;

import com.google.template.soy.base.SoySyntaxException;

/**
 * QuieterSoySyntaxException. Doesn't fill in the stack trace, so you don't have to read it!
 *
 * @since v2.1
 */
public class QuieterSoySyntaxException extends SoySyntaxException
{
    public QuieterSoySyntaxException(final String message)
    {
        super(message);
    }

    public java.lang.Throwable fillInStackTrace()
    {
        return this;
    }
}
