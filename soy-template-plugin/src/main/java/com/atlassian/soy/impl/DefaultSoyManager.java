package com.atlassian.soy.impl;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.soy.impl.data.SoyDataConverter;
import com.atlassian.soy.impl.modules.GuiceModuleSupplier;
import com.atlassian.soy.renderer.SoyException;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.collect.ComputationException;
import com.google.common.io.Closeables;
import com.google.inject.Injector;
import com.google.template.soy.SoyFileSet;
import com.google.template.soy.base.SoySyntaxException;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.msgs.SoyMsgBundle;
import com.google.template.soy.parseinfo.SoyTemplateInfo;
import com.google.template.soy.shared.SoyCssRenamingMap;
import com.google.template.soy.tofu.SoyTofu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.atlassian.soy.impl.DevMode.isDevMode;


public class DefaultSoyManager implements SoyManager
{
    private static final SoyTofu DIDNOTCOMPILE = new NullTofu();
    
    private static final Logger log = LoggerFactory.getLogger(DefaultSoyManager.class);
    public static final int INJECTOR_CACHE_EXPIRY_TIME = 45;
    public static final TimeUnit INJECTOR_CACHE_EXPIRY_TIME_UNIT = TimeUnit.MINUTES;

    private final SoyDataConverter soyDataConverter;

    /**
     * Compiled Soy templates keyed on complete plugin-module key.
     */
    private final Cache<String, SoyTofu> soyTofuCache;
    /** Cache of the most recently modified time of the soy files in a plugin-module */
    private final Cache<String, Long> lastModifiedCache;
    private final TemplateSetFactory templateSetFactory;
    private final SoyDependencyInjectorFactory soyDependencyInjectorFactory;

    public DefaultSoyManager(PluginAccessor pluginAccessor,
                             PluginEventManager pluginEventManager,
                             ServletContextFactory servletContextFactory,
                             GuiceModuleSupplier moduleSupplier,
                             SoyDataConverter soyDataConverter)
    {
        this.soyDataConverter = soyDataConverter;

        soyTofuCache = CacheBuilder.newBuilder().build(new CacheLoader<String, SoyTofu>()
        {
            @Override
            public SoyTofu load(String key) throws SoyException
            {
                SoyTofu soyTofu = strainTofu(key);
                if (soyTofu != null)
                {
                    return soyTofu;
                }
                else
                {
                    return DIDNOTCOMPILE;
                }
            }
        });
        lastModifiedCache = CacheBuilder.newBuilder().build(new CacheLoader<String, Long>()
        {
            @Override
            public Long load(String key)
            {
                return getLastModifiedForModule(key);
            }
        });

        soyDependencyInjectorFactory = new SoyDependencyInjectorFactory(moduleSupplier);
        templateSetFactory = new TemplateSetFactory(pluginAccessor, servletContextFactory, INJECTOR_CACHE_EXPIRY_TIME, INJECTOR_CACHE_EXPIRY_TIME_UNIT);
        pluginEventManager.register(this);
    }

    @Override
    public void render(Appendable appendable, String completeModuleKey, String templateName, Map<String, Object> data, Map<String, Object> injectedData) throws SoyException
    {
        if (isDevMode())
        {
            log.debug("Clearing caches in dev mode");
            clearCaches(completeModuleKey);
        }

        try
        {
            SoyTofu tofu = soyTofuCache.getUnchecked(completeModuleKey);
            if (tofu == DIDNOTCOMPILE)
            {
                // Will only occur if there is a Soy exception compiling one of the templates for
                // this module.
                throw new SoyException("Unable to compile Soy template in plugin module: " + completeModuleKey);
            }
            tofu.newRenderer(templateName)
                    .setData(soyDataConverter.convertToSoyMapData(data))
                    .setIjData(soyDataConverter.convertToSoyMapData(injectedData))
                    .render(appendable);
        }
        catch (ComputationException e)
        {
            throw new SoyException("Unable to compile Soy template in plugin module: " + completeModuleKey, e.getCause());
        }
    }

    @SuppressWarnings ("UnusedDeclaration")
    @PluginEventListener
    public void pluginModuleEnabled(PluginModuleEnabledEvent event)
    {
        clearCaches(null);
    }

    @SuppressWarnings ("UnusedDeclaration")
    @PluginEventListener
    public void pluginModuleDisabled(PluginModuleDisabledEvent event)
    {
        clearCaches(null);
    }

    /**
     * @param completeModuleKey if <code>null</code>, will clear entire soy tofu cache
     */
    private void clearCaches(String completeModuleKey)
    {
        soyDependencyInjectorFactory.clear();
        templateSetFactory.clear();

        if (completeModuleKey == null)
        {
            soyTofuCache.invalidateAll();
        }
        else if (isModified(completeModuleKey))
        {
            soyTofuCache.invalidate(completeModuleKey);
            // This is the last time it has been compiled - we need to clear it
            lastModifiedCache.invalidate(completeModuleKey);
        }
    }

    private long getLastModifiedForModule(String completeModuleKey)
    {
        long lastModified = 0;
        for(URL url : templateSetFactory.get(completeModuleKey))
        {
            lastModified = Math.max(lastModified, getLastModified(url));
        }
        return lastModified;
    }

    private boolean isModified(String completeModuleKey)
    {
        final Long previousModifiedDate = lastModifiedCache.getUnchecked(completeModuleKey);
        final long currentModifiedDate = getLastModifiedForModule(completeModuleKey);
        return previousModifiedDate < currentModifiedDate || currentModifiedDate == -1;
    }

    private static long getLastModified(URL url)
    {
        try
        {
            URLConnection urlConnection = url.openConnection();
            try {
                return urlConnection.getLastModified();
            } finally {
                // Don't leak underlying file handles
                Closeables.closeQuietly(urlConnection.getInputStream());
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    /*
     * Rebuilds the tofu that represents all the current soy web resource files registered in the plugin system.
     */
    private SoyTofu strainTofu(String completeModuleKey) throws SoyException
    {
        SoyFileSet.Builder builder = makeSoyFileSetBuilder();

        for (URL file : templateSetFactory.get(completeModuleKey))
        {
            builder.add(file);
        }

        try
        {
            return builder.build().compileToTofu();
        }
        catch (SoySyntaxException e)
        {
            if (isDevMode())
            {
                throw new QuieterSoySyntaxException(e.getMessage());
            }
            else
            {
                throw e;
            }
        }
    }

    public SoyFileSet.Builder makeSoyFileSetBuilder()
    {
        Injector injector = soyDependencyInjectorFactory.get();
        return injector.getInstance(SoyFileSet.Builder.class);
    }

    /*
        Placeholder SoyTofu implementation to go into ConcurrentMap that can't take null.
     */
    private static class NullTofu implements SoyTofu
    {
        @Override
        public String getNamespace()
        {
            return null;
        }

        @Override
        public SoyTofu forNamespace(String namespace)
        {
            return null;
        }

        @Override
        public String render(SoyTemplateInfo templateInfo, Map<String, ?> data, SoyMsgBundle msgBundle)
        {
            return null;
        }

        @Override
        public String render(SoyTemplateInfo templateInfo, SoyMapData data, SoyMsgBundle msgBundle)
        {
            return null;
        }

        @Override
        public String render(String templateName, Map<String, ?> data, SoyMsgBundle msgBundle)
        {
            return null;
        }

        @Override
        public String render(String templateName, SoyMapData data, SoyMsgBundle msgBundle)
        {
            return null;
        }

        @Override
        public boolean isCaching() {
            return false;
        }

        @Override
        public void addToCache(SoyMsgBundle soyMsgBundle, SoyCssRenamingMap soyCssRenamingMap) {
        }

        @Override
        public Renderer newRenderer(SoyTemplateInfo soyTemplateInfo) {
            return null;
        }

        @Override
        public Renderer newRenderer(String s) {
            return null;
        }
    }

}