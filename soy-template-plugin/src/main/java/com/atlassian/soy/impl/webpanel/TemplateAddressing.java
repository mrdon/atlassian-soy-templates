package com.atlassian.soy.impl.webpanel;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.soy.impl.DevMode;
import com.atlassian.soy.renderer.SoyException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang.StringUtils.substringAfter;
import static org.apache.commons.lang.StringUtils.substringBefore;

/**
 * Encapsulates the code that takes a Soy template address and returns the valid parts
 */
class TemplateAddressing
{
    private static final Logger log = LoggerFactory.getLogger(TemplateAddressing.class);

    static class Address
    {
        private final ModuleCompleteKey completeKey;
        private final String templateName;

        Address(final ModuleCompleteKey completeKey, final String templateName)
        {
            this.completeKey = completeKey;
            this.templateName = templateName;
        }

        public ModuleCompleteKey getCompleteKey()
        {
            return completeKey;
        }

        public String getTemplateName()
        {
            return templateName;
        }
    }

    /**
     * Parses the passed in address and splits it into completeModuleKey and Soy template address
     *
     * @param templateAddress the address that the developer has provided
     * @param callingPluginKey the key of the calling plugin.  This can be used for defaulting reasons
     * @return completeModuleKey and Soy template address
     */
    public static Address parseTemplateAddress(final String templateAddress, final String callingPluginKey)
            throws SoyException
    {
        Address address;
        try
        {
            address = parseAtlassianTemplateAddress(templateAddress, callingPluginKey);
        }
        catch (SoyException e)
        {
            //
            // The Stash boys used pluginKey:moduleKey,templateName.  So that they can migrate to to the common
            // code we have left their parsing in as is.  Its not so bad really but we only report on the new official
            // addressing format and we should only document the new.
            //
            address = parseStashCompatibleTemplateAddress(templateAddress);
        }
        return address;
    }

    /*
     * Must be in form pluginKey:moduleKey/templateName or :moduleKey/templateName
     */
    private static Address parseAtlassianTemplateAddress(final String templateAddress, final String callingPluginKey)
            throws SoyException
    {
        String completeKey = substringBefore(templateAddress, "/");
        String templateName = substringAfter(templateAddress, "/");
        if (isEmpty(completeKey) || isEmpty(templateName))
        {
            throw badTemplateName(templateAddress);
        }

        if (StringUtils.countMatches(completeKey, ":") != 1)
        {
            throw badTemplateName(templateAddress);
        }
        String pluginKey = substringBefore(completeKey, ":");
        String moduleKey = substringAfter(completeKey, ":");
        // we allow defaulting
        if (isEmpty(pluginKey) || ".".equals(pluginKey))
        {
            pluginKey = callingPluginKey;
        }
        if (isEmpty(pluginKey) || isEmpty(moduleKey))
        {
            throw badTemplateName(templateAddress);
        }
        return new Address(new ModuleCompleteKey(pluginKey, moduleKey), templateName);
    }


    //
    // Taken directly from original Stash code
    //
    private static Address parseStashCompatibleTemplateAddress(final String templateAddress) throws SoyException
    {
        final String[] parts = templateAddress.split(",", 2);
        if (parts.length != 2)
        {
            throw badTemplateName(templateAddress);
        }
        if (DevMode.isDevMode())
        {
            log.warn("Stash style pluginKey:moduleKey,templateName addressing is deprecated and will be removed in the future.  Please update to pluginKey:moduleKey/templateName addressing");
        }
        return new Address(new ModuleCompleteKey(parts[0]), parts[1]);
    }


    private static SoyException badTemplateName(final String templateAddress)
    {
        return new SoyException(String.format("Template name must be in the form 'pluginKey:moduleKey/templateAddress' - '%s'", templateAddress));
    }

}
