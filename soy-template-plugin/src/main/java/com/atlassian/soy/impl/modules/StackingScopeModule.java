package com.atlassian.soy.impl.modules;

import com.google.inject.AbstractModule;
import com.google.template.soy.shared.internal.ApiCallScope;
import com.google.template.soy.shared.internal.GuiceSimpleScope;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations;

class StackingScopeModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        // Create the API call scope.
        GuiceSimpleScope apiCallScope = new GuiceStackingScope();
        bindScope(ApiCallScope.class, apiCallScope);
        // Make the API call scope instance injectable.
        bind(GuiceSimpleScope.class).annotatedWith(ApiCallScopeBindingAnnotations.ApiCall.class)
                .toInstance(apiCallScope);
    }
}
